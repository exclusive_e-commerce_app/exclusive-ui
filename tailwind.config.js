/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {},
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
      xxl: '1920px'
    },
    container: {
      center: true,
    },
    colors: {
      'transparent': '#00000000',
      'black': '#000000',
      'white': '#FFFFFF',
      'white-f5': '#F5F5F5',
      'orange-red': '#DB4444',
      'orange-red-hover': '#E07575',
      'lime': '#00FF66',
      'gray-04': '#00000067',
      'gray-03': '#0000004D',
      'gray': '#2F2E30',
      'gray-light': '#7D8184',
      'gold': '#FFAD33',
    },
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}

