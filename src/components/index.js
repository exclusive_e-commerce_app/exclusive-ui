import TopHeaderComponent from "@/components/TopHeaderComponent.vue";
import HeaderComponent from '@/components/HeaderComponent.vue'
import ContainerComponent from '@/components/ContainerComponent.vue'
import SideBarComponent from '@/components/SideBarComponent.vue'
import CarouselComponent from '@/components/CarouselComponent.vue'

export default [
    {name: 'TopHeaderComponent', self: TopHeaderComponent},
    {name: 'HeaderComponent', self: HeaderComponent},
    {name: 'ContainerComponent', self: ContainerComponent},
    {name: 'SideBarComponent', self: SideBarComponent},
    {name: 'CarouselComponent', self: CarouselComponent},
]